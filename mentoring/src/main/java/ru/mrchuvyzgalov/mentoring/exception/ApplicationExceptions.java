package ru.mrchuvyzgalov.mentoring.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
public enum ApplicationExceptions {
    CATEGORY_NOT_FOUND("category not found", 404),
    CATEGORY_IS_EXIST("category is exist", 404),

    SKILL_NOT_FOUND("skill not found", 404),
    SKILL_IS_EXIST("skill is exist", 404),
    SKILL_NOT_VALID("skill not valid", 404),

    PROJECT_NOT_FOUND("project not found", 404),
    PROJECT_IS_EXIST("project is exist", 404),

    TIME_RANGE_NOT_FOUND("time range not found", 404),
    TIME_RANGE_IS_EXIST("time range is exist", 404),
    TIME_RANGE_NOT_VALID("time range not valid", 404),

    MENTOR_NOT_FOUND("mentor not found", 404),
    MENTOR_IS_EXIST("mentor is exist", 404),
    MENTOR_NOT_VALID("mentor not valid", 404),
    MENTOR_AND_TIME_RANGE_NOT_FOUND("mentor and time range not found", 404),

    MENTEE_NOT_FOUND("mentee not found", 404),
    MENTEE_IS_EXIST("mentee is exist", 404),
    MENTEE_NOT_VALID("mentee not valid", 404),

    APPLICATION_NOT_FOUND("application not found", 404),
    APPLICATION_IS_EXIST("application is exist", 404),
    APPLICATION_NOT_VALID("application is not valid", 404),

    DATA_NOT_VALID("data not valid", 404);

    private final String message;
    private final int code;

    ApplicationExceptions(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception() {
        return new ApplicationException(this);
    }

    public static class ApplicationException extends RuntimeException {
        public final ApplicationExceptionCompanion companion;

        public ApplicationException(ApplicationExceptions error) {
            super(error.message);
            this.companion = new ApplicationExceptionCompanion(error.code, error.message);
        }

        @AllArgsConstructor
        @Data
        public static class ApplicationExceptionCompanion {
            private int code;
            private String message;
        }
    }
}
