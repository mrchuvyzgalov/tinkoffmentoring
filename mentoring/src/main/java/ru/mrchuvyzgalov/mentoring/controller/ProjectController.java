package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.Project;
import ru.mrchuvyzgalov.mentoring.model.dto.ProjectDto;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.ProjectIdContainsInDbConstraint;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/project")
@AllArgsConstructor
@Validated
public class ProjectController {
    private final ProjectService projectService;

    @PostMapping("/create")
    public Project createNewProject(@Valid @RequestBody ProjectDto projectDto) {
        Project project = new Project(UUID.randomUUID(), projectDto.getName(), projectDto.getDescription());
        projectService.save(project);
        return project;
    }

    @PutMapping("/update")
    public void updateProject(@ProjectIdContainsInDbConstraint @RequestParam UUID id, @Valid @RequestBody ProjectDto projectDto) {
        projectService.updateById(new Project(id, projectDto.getName(), projectDto.getDescription()));
    }

    @DeleteMapping("/delete")
    public void deleteSkill(@ProjectIdContainsInDbConstraint @RequestParam UUID id) {
        projectService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllProjects() {
        projectService.deleteAll();
    }

    @GetMapping("/get")
    public Project getProject(@ProjectIdContainsInDbConstraint @RequestParam UUID id) {
        return projectService.findById(id);
    }

    @GetMapping("/get-all")
    public List<Project> getAllProjects() {
        return projectService.findAll();
    }
}
