package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.Mentor;
import ru.mrchuvyzgalov.mentoring.model.Project;
import ru.mrchuvyzgalov.mentoring.model.Skill;
import ru.mrchuvyzgalov.mentoring.model.TimeRange;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorDto;
import ru.mrchuvyzgalov.mentoring.service.MentorService;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;
import ru.mrchuvyzgalov.mentoring.service.SkillService;
import ru.mrchuvyzgalov.mentoring.service.TimeRangeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.CategoryIdContainsInDbConstraint;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MentorIdContainsInDbConstraint;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.SkillIdContainsInDbConstraint;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/mentor")
@AllArgsConstructor
@Validated
public class MentorController {
    private final MentorService mentorService;
    private final ProjectService projectService;
    private final SkillService skillService;
    private final TimeRangeService timeRangeService;

    @PostMapping("/create")
    public Mentor createNewMentor(@Valid @RequestBody MentorDto mentorDto) {
        Project project = mentorDto.getProjectId() == null ?
                null : projectService.findById(mentorDto.getProjectId());

        List<Skill> skills = new ArrayList<>();
        for (UUID skillId : mentorDto.getSkillIds()) {
            skills.add(skillService.findById(skillId));
        }

        Set<TimeRange> timeRanges = new HashSet<>();
        for (UUID timeRangeId : mentorDto.getTimeRangeIds()) {
            timeRanges.add(timeRangeService.findById(timeRangeId));
        }

        Mentor mentor = new Mentor(UUID.randomUUID(), mentorDto.getSurname(), mentorDto.getName(),
                mentorDto.getPatronymic(), project, skills, mentorDto.getPhoneNumber(),
                timeRanges);

        mentorService.save(mentor);
        return mentor;
    }

    @PutMapping("/update")
    public void updateMentor(@MentorIdContainsInDbConstraint @RequestParam UUID id, @Valid @RequestBody MentorDto mentorDto) {
        Project project = mentorDto.getProjectId() == null ?
                null : projectService.findById(mentorDto.getProjectId());

        List<Skill> skills = new ArrayList<>();
        for (UUID skillId : mentorDto.getSkillIds()) {
            skills.add(skillService.findById(skillId));
        }

        Set<TimeRange> timeRanges = new HashSet<>();
        for (UUID timeRangeId : mentorDto.getTimeRangeIds()) {
            timeRanges.add(timeRangeService.findById(timeRangeId));
        }

        mentorService.updateById(new Mentor(id, mentorDto.getSurname(), mentorDto.getName(),
                mentorDto.getPatronymic(), project, skills, mentorDto.getPhoneNumber(),
                timeRanges));
    }

    @DeleteMapping("/delete")
    public void deleteMentor(@MentorIdContainsInDbConstraint @RequestParam UUID id) {
        mentorService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllMentors() {
        mentorService.deleteAll();
    }

    @GetMapping("/get")
    public Mentor getMentor(@MentorIdContainsInDbConstraint @RequestParam UUID id) {
        return mentorService.findById(id);
    }

    @GetMapping("/get-by-skill")
    public List<Mentor> getMentorsBySkill(@SkillIdContainsInDbConstraint @RequestParam UUID id) {
        return mentorService.findBySkill(id);
    }

    @GetMapping("/get-by-category")
    public List<Mentor> getMentorsByCategory(@CategoryIdContainsInDbConstraint @RequestParam UUID id) {
        return mentorService.findByCategory(id);
    }

    @GetMapping("/get-all")
    public List<Mentor> getAllMentors() {
        return mentorService.findAll();
    }
}
