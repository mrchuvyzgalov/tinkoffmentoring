package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.Category;
import ru.mrchuvyzgalov.mentoring.model.Skill;
import ru.mrchuvyzgalov.mentoring.model.dto.SkillDto;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.service.SkillService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.SkillIdContainsInDbConstraint;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/skill")
@AllArgsConstructor
@Validated
public class SkillController {
    private final SkillService skillService;
    private final CategoryService categoryService;

    @PostMapping("/create")
    public Skill createNewSkill(@Valid @RequestBody SkillDto skillDto) {
        Category category = categoryService.findById(skillDto.getCategoryId());
        Skill skill = new Skill(UUID.randomUUID(), skillDto.getName(), category, skillDto.getQualification(), skillDto.getDescription());
        skillService.save(skill);
        return skill;
    }

    @PutMapping("/update")
    public void updateSkill(@SkillIdContainsInDbConstraint @RequestParam UUID id, @Valid @RequestBody SkillDto skillDto) {
        Category category = categoryService.findById(skillDto.getCategoryId());
        skillService.updateById(new Skill(id, skillDto.getName(), category, skillDto.getQualification(), skillDto.getDescription()));
    }

    @DeleteMapping("/delete")
    public void deleteSkill(@SkillIdContainsInDbConstraint @RequestParam UUID id) {
        skillService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllSkills() {
        skillService.deleteAll();
    }

    @GetMapping("/get")
    public Skill getSkill(@SkillIdContainsInDbConstraint @RequestParam UUID id) {
        return skillService.findById(id);
    }

    @GetMapping("/get-all")
    public List<Skill> getAllSkills() {
        return skillService.findAll();
    }
}
