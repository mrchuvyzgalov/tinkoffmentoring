package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.Mentee;
import ru.mrchuvyzgalov.mentoring.model.Project;
import ru.mrchuvyzgalov.mentoring.model.dto.MenteeDto;
import ru.mrchuvyzgalov.mentoring.service.MenteeService;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MenteeIdContainsInDbConstraint;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/mentee")
@AllArgsConstructor
@Validated
public class MenteeController {
    private final MenteeService menteeService;
    private final ProjectService projectService;

    @PostMapping("/create")
    public Mentee createNewMentee(@RequestBody MenteeDto menteeDto) {
        Project project = menteeDto.getProjectId() == null ?
                null : projectService.findById(menteeDto.getProjectId());

        Mentee mentee = new Mentee(UUID.randomUUID(), menteeDto.getSurname(),
                menteeDto.getName(), menteeDto.getPatronymic(), project);

        menteeService.save(mentee);
        return mentee;
    }

    @PutMapping("/update")
    public void updateMentee(@MenteeIdContainsInDbConstraint @RequestParam UUID id, @RequestBody MenteeDto menteeDto) {
        Project project = menteeDto.getProjectId() == null ?
                null : projectService.findById(menteeDto.getProjectId());
        menteeService.updateById(new Mentee(id, menteeDto.getSurname(),
                menteeDto.getName(), menteeDto.getPatronymic(), project));
    }

    @DeleteMapping("/delete")
    public void deleteMentee(@MenteeIdContainsInDbConstraint @RequestParam UUID id) {
        menteeService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllMentees() {
        menteeService.deleteAll();
    }

    @GetMapping("/get")
    public Mentee getMentee(@MenteeIdContainsInDbConstraint @RequestParam UUID id) {
        return menteeService.findById(id);
    }

    @GetMapping("/get-all")
    public List<Mentee> getAllMentees() {
        return menteeService.findAll();
    }
}
