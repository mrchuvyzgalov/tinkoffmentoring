package ru.mrchuvyzgalov.mentoring.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.ApplicationException;
import ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.ApplicationException.ApplicationExceptionCompanion;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ApplicationExceptionCompanion> handleApplicationException(ApplicationException e) {
        return ResponseEntity.status(e.companion.getCode()).body(e.companion);
    }
}
