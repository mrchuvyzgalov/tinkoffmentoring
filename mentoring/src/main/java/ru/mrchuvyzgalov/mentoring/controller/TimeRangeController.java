package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.TimeRange;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorTimeRangeDto;
import ru.mrchuvyzgalov.mentoring.model.dto.TimeRangeDto;
import ru.mrchuvyzgalov.mentoring.service.TimeRangeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MentorIdContainsInDbConstraint;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MentorIdTimeRangeIdContainsInDbConstraint;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.TimeRangeIdContainsInDbConstraint;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/time-range")
@AllArgsConstructor
@Validated
public class TimeRangeController {
    private final TimeRangeService timeRangeService;

    @PostMapping("/create")
    public TimeRange createNewTimeRange(@Valid @RequestBody TimeRangeDto timeRangeDto) {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), timeRangeDto.getStartHour(),
                timeRangeDto.getStartMinutes(), timeRangeDto.getFinishHour(), timeRangeDto.getFinishMinutes(),
                timeRangeDto.getDayOfWeek());
        timeRangeService.save(timeRange);

        return timeRange;
    }

    @PutMapping("/update")
    public void updateTimeRange(@TimeRangeIdContainsInDbConstraint @RequestParam UUID id, @Valid @RequestBody TimeRangeDto timeRangeDto) {
        timeRangeService.updateById(new TimeRange(id, timeRangeDto.getStartHour(),
                timeRangeDto.getStartMinutes(), timeRangeDto.getFinishHour(), timeRangeDto.getFinishMinutes(),
                timeRangeDto.getDayOfWeek()));
    }

    @DeleteMapping("/delete")
    public void deleteTimeRange(@TimeRangeIdContainsInDbConstraint @RequestParam UUID id) {
        timeRangeService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllTimeRanges() {
        timeRangeService.deleteAll();
    }

    @GetMapping("/get")
    public TimeRange getTimeRange(@TimeRangeIdContainsInDbConstraint @RequestParam UUID id) {
        return timeRangeService.findById(id);
    }

    @GetMapping("/get-free-time-range")
    public Set<TimeRange> getFreeTimeRanges(@MentorIdContainsInDbConstraint @RequestParam UUID mentorId) {
        return timeRangeService.findFreeTimeRangeByMentorId(mentorId);
    }

    @GetMapping("/get-all")
    public List<TimeRange> getAllTimeRanges() {
        return timeRangeService.findAll();
    }

    @PutMapping("/book-time-range")
    public void bookTimeRangeWithIdAndMentorId(@MentorIdTimeRangeIdContainsInDbConstraint @RequestBody MentorTimeRangeDto mentorTimeRange) {
        timeRangeService.bookTimeRangeWithIdAndMentorId(mentorTimeRange);
    }
}
