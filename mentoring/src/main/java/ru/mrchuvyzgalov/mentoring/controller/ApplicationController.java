package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.*;
import ru.mrchuvyzgalov.mentoring.model.dto.ApplicationDto;
import ru.mrchuvyzgalov.mentoring.service.*;
import ru.mrchuvyzgalov.mentoring.validation.annotation.ApplicationAcceptOrRejectConstraint;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.ApplicationIdContainsInDbConstraint;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/application")
@AllArgsConstructor
@Validated
public class ApplicationController {
    private final ApplicationService applicationService;
    private final MentorService mentorService;
    private final MenteeService menteeService;
    private final SkillService skillService;
    private final TimeRangeService timeRangeService;

    @PostMapping("/create")
    public Application createNewApplication(@Valid @RequestBody ApplicationDto applicationDto) {
        Mentor mentor = mentorService.findById(applicationDto.getMentorId());
        Mentee mentee = menteeService.findById(applicationDto.getMenteeId());
        TimeRange timeRange = timeRangeService.findById(applicationDto.getTimeRangeId());
        Skill skill = skillService.findById(applicationDto.getMentorSkillId());

        Application application = new Application(UUID.randomUUID(), mentee, mentor, timeRange, skill, Application.State.CREATED);
        applicationService.save(application);
        return application;
    }

    @PutMapping("/accept")
    public void acceptApplication(@ApplicationAcceptOrRejectConstraint @RequestParam UUID id) {
        applicationService.acceptApplication(id);
    }

    @PutMapping("/reject")
    public void rejectApplication(@ApplicationAcceptOrRejectConstraint @RequestParam UUID id) {
        applicationService.rejectApplication(id);
    }

    @PutMapping("/update")
    public void updateApplication(@ApplicationIdContainsInDbConstraint @RequestParam UUID id, @Valid @RequestBody ApplicationDto applicationDto) {
        Application.State state = applicationService.findById(id).getState();
        Mentor mentor = mentorService.findById(applicationDto.getMentorId());
        Mentee mentee = menteeService.findById(applicationDto.getMenteeId());
        TimeRange timeRange = timeRangeService.findById(applicationDto.getTimeRangeId());
        Skill skill = skillService.findById(applicationDto.getMentorSkillId());

        applicationService.updateById(new Application(id, mentee, mentor, timeRange, skill, state));
    }

    @DeleteMapping("/delete")
    public void deleteApplication(@ApplicationIdContainsInDbConstraint @RequestParam UUID id) {
        applicationService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllApplications() {
        applicationService.deleteAll();
    }

    @GetMapping("/get")
    public Application getApplication(@ApplicationIdContainsInDbConstraint @RequestParam UUID id) {
        return applicationService.findById(id);
    }

    @GetMapping("/get-created")
    public List<Application> getCreatedApplications() {
        return applicationService.findCreatedApplications();
    }

    @GetMapping("/get-all")
    public List<Application> getAllApplications() {
        return applicationService.findAll();
    }
}
