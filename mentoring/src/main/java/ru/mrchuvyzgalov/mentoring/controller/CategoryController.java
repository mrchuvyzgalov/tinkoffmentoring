package ru.mrchuvyzgalov.mentoring.controller;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.mentoring.model.Category;
import ru.mrchuvyzgalov.mentoring.model.dto.CategoryDto;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.CategoryIdContainsInDbConstraint;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/category")
@AllArgsConstructor
@Validated
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/create")
    public Category createNewCategory(@Valid @RequestBody CategoryDto categoryDto) {
        Category category = new Category(UUID.randomUUID(), categoryDto.getName(), categoryDto.getDescription());
        categoryService.save(category);
        return category;
    }

    @PutMapping("/update")
    public void updateCategory(@CategoryIdContainsInDbConstraint @RequestParam UUID id, @Valid @RequestBody CategoryDto categoryDto) {
        categoryService.updateById(new Category(id, categoryDto.getName(), categoryDto.getDescription()));
    }

    @DeleteMapping("/delete")
    public void deleteCategory(@CategoryIdContainsInDbConstraint @RequestParam UUID id) {
        categoryService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllCategories() {
        categoryService.deleteAll();
    }

    @GetMapping("/get")
    public Category getCategory(@CategoryIdContainsInDbConstraint @RequestParam UUID id) {
        return categoryService.findById(id);
    }

    @GetMapping("/get-all")
    public List<Category> getAllCategories() {
        return categoryService.findAll();
    }
}
