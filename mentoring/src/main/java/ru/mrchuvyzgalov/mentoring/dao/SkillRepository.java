package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.Skill;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface SkillRepository {
    void save(Skill skill);

    Optional<Skill> findById(UUID id);
    List<Skill> findAll();

    void updateById(Skill skill);

    void deleteById(UUID id);
    void deleteAll();
}
