package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorTimeRangeDto;
import ru.mrchuvyzgalov.mentoring.model.TimeRange;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Mapper
public interface TimeRangeRepository {
    void save(TimeRange timeRange);

    Optional<TimeRange> findById(UUID id);
    List<TimeRange> findAll();
    Set<TimeRange> findFreeTimeRangeByMentorId(UUID mentorId);

    void updateById(TimeRange timeRange);

    void deleteById(UUID id);
    void deleteAll();

    void bookTimeRangeWithIdAndMentorId(MentorTimeRangeDto mentorTimeRange);
}
