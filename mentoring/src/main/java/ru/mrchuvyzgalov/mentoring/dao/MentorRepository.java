package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.Mentor;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface MentorRepository {
    void save(Mentor mentor);

    Optional<Mentor> findById(UUID id);
    List<Mentor> findBySkill(UUID id);
    List<Mentor> findByCategory(UUID id);
    List<Mentor> findAll();

    void updateById(Mentor mentor);

    void deleteById(UUID id);
    void deleteAll();
}
