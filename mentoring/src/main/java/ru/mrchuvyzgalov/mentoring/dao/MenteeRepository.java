package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.Mentee;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface MenteeRepository {
    void save(Mentee mentee);

    Optional<Mentee> findById(UUID id);
    List<Mentee> findAll();

    void updateById(Mentee mentee);

    void deleteById(UUID id);
    void deleteAll();
}
