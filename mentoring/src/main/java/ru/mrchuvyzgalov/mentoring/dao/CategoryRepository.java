package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.Category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface CategoryRepository {
    void save(Category category);

    Optional<Category> findById(UUID id);
    List<Category> findAll();

    void updateById(Category category);

    void deleteById(UUID id);
    void deleteAll();
}
