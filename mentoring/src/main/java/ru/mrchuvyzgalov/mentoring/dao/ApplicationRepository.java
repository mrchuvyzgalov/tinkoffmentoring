package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.Application;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface ApplicationRepository {
    void save(Application application);

    Optional<Application> findById(UUID id);
    List<Application> findCreatedApplications();
    List<Application> findAll();

    void updateById(Application application);

    void deleteById(UUID id);
    void deleteAll();
}
