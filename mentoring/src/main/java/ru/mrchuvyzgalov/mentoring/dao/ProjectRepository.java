package ru.mrchuvyzgalov.mentoring.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.mentoring.model.Project;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface ProjectRepository {
    void save(Project project);

    Optional<Project> findById(UUID id);
    List<Project> findAll();

    void updateById(Project project);

    void deleteById(UUID id);
    void deleteAll();
}
