package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorTimeRangeDto;
import ru.mrchuvyzgalov.mentoring.service.MentorService;
import ru.mrchuvyzgalov.mentoring.service.TimeRangeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MentorIdTimeRangeIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.*;

@AllArgsConstructor
public class MentorIdTimeRangeIdContainsInDbConstraintValidator implements ConstraintValidator<MentorIdTimeRangeIdContainsInDbConstraint, MentorTimeRangeDto> {
    private MentorService mentorService;
    private TimeRangeService timeRangeService;

    @Override
    public boolean isValid(MentorTimeRangeDto mentorTimeRangeDto, ConstraintValidatorContext constraintValidatorContext) {
        boolean mentorInDb = mentorService.contains(mentorTimeRangeDto.getMentorId());
        boolean timeRangeInDb = timeRangeService.contains(mentorTimeRangeDto.getTimeRangeId());
        if (mentorInDb && timeRangeInDb) {
            return true;
        }

        if (mentorInDb) {
            throw TIME_RANGE_NOT_FOUND.exception();
        }
        else if (timeRangeInDb) {
            throw MENTOR_NOT_FOUND.exception();
        }
        else {
            throw MENTOR_AND_TIME_RANGE_NOT_FOUND.exception();
        }
    }
}
