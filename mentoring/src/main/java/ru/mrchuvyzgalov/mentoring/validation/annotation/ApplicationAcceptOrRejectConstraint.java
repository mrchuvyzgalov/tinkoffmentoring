package ru.mrchuvyzgalov.mentoring.validation.annotation;

import ru.mrchuvyzgalov.mentoring.validation.validator.ApplicationAcceptOrRejectConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ApplicationAcceptOrRejectConstraintValidator.class)
public @interface ApplicationAcceptOrRejectConstraint {
    String message() default "Application dto not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
