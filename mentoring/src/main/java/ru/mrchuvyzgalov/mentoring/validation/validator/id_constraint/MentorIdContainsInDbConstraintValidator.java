package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.service.MentorService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MentorIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.MENTOR_NOT_FOUND;

@AllArgsConstructor
public class MentorIdContainsInDbConstraintValidator implements ConstraintValidator<MentorIdContainsInDbConstraint, UUID> {
    private MentorService mentorService;

    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (mentorService.contains(uuid)) {
            return true;
        }
        else {
            throw MENTOR_NOT_FOUND.exception();
        }
    }
}
