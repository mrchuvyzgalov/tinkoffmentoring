package ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.Mentor;
import ru.mrchuvyzgalov.mentoring.model.Skill;
import ru.mrchuvyzgalov.mentoring.model.TimeRange;
import ru.mrchuvyzgalov.mentoring.model.dto.ApplicationDto;
import ru.mrchuvyzgalov.mentoring.service.MenteeService;
import ru.mrchuvyzgalov.mentoring.service.MentorService;
import ru.mrchuvyzgalov.mentoring.service.SkillService;
import ru.mrchuvyzgalov.mentoring.service.TimeRangeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.ApplicationDtoConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.APPLICATION_NOT_VALID;

@AllArgsConstructor
public class ApplicationDtoConstraintValidator implements ConstraintValidator<ApplicationDtoConstraint, ApplicationDto> {
    private MenteeService menteeService;
    private MentorService mentorService;
    private TimeRangeService timeRangeService;
    private SkillService skillService;

    @Override
    public boolean isValid(ApplicationDto applicationDto, ConstraintValidatorContext constraintValidatorContext) {
        if (menteeService.contains(applicationDto.getMenteeId()) && mentorService.contains(applicationDto.getMentorId())
            && timeRangeService.contains(applicationDto.getTimeRangeId()) && skillService.contains(applicationDto.getMentorSkillId())) {

            TimeRange timeRange = timeRangeService.findById(applicationDto.getTimeRangeId());
            Skill skill = skillService.findById(applicationDto.getMentorSkillId());
            Mentor mentor = mentorService.findById(applicationDto.getMentorId());

            if (mentor.getSkills().contains(skill) && timeRangeService.findFreeTimeRangeByMentorId(mentor.getId()).contains(timeRange)) {
                return true;
            }
            else {
                throw APPLICATION_NOT_VALID.exception();
            }
        }
        throw APPLICATION_NOT_VALID.exception();
    }
}
