package ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint;

import ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint.MenteeDtoConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MenteeDtoConstraintValidator.class)
public @interface MenteeDtoConstraint {
    String message() default "Mentee dto not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
