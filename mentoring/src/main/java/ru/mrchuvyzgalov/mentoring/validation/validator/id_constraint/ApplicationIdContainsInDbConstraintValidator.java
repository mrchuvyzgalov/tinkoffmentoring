package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.service.ApplicationService;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.ApplicationIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.APPLICATION_NOT_FOUND;

@AllArgsConstructor
public class ApplicationIdContainsInDbConstraintValidator implements ConstraintValidator<ApplicationIdContainsInDbConstraint, UUID> {
    private ApplicationService applicationService;
    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (applicationService.contains(uuid)) {
            return true;
        }
        else {
            throw APPLICATION_NOT_FOUND.exception();
        }
    }
}
