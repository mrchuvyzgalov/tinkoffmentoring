package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.service.SkillService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.SkillIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.SKILL_NOT_FOUND;

@AllArgsConstructor
public class SkillIdContainsInDbConstraintValidator implements ConstraintValidator<SkillIdContainsInDbConstraint, UUID> {
    private SkillService skillService;

    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (skillService.contains(uuid)) {
            return true;
        }
        else {
            throw SKILL_NOT_FOUND.exception();
        }
    }
}
