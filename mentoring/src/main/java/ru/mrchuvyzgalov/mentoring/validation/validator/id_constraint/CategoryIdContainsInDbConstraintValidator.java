package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.CategoryIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.CATEGORY_NOT_FOUND;

@AllArgsConstructor
public class CategoryIdContainsInDbConstraintValidator implements ConstraintValidator<CategoryIdContainsInDbConstraint, UUID> {
    private CategoryService categoryService;

    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (categoryService.contains(uuid)) {
            return true;
        }
        else {
            throw CATEGORY_NOT_FOUND.exception();
        }
    }
}
