package ru.mrchuvyzgalov.mentoring.validation.validator;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.Application;
import ru.mrchuvyzgalov.mentoring.service.*;
import ru.mrchuvyzgalov.mentoring.validation.annotation.ApplicationAcceptOrRejectConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.APPLICATION_NOT_FOUND;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.APPLICATION_NOT_VALID;

@AllArgsConstructor
public class ApplicationAcceptOrRejectConstraintValidator implements ConstraintValidator<ApplicationAcceptOrRejectConstraint, UUID> {
    private ApplicationService applicationService;

    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (!applicationService.contains(uuid)) {
            throw APPLICATION_NOT_FOUND.exception();
        }
        if (applicationService.findById(uuid).getState().equals(Application.State.CREATED)) {
            return true;
        }
        else {
            throw APPLICATION_NOT_VALID.exception();
        }
    }
}
