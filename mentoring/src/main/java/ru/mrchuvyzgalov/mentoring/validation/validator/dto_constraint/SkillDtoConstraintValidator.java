package ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.dto.SkillDto;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.SkillDtoConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.SKILL_NOT_VALID;

@AllArgsConstructor
public class SkillDtoConstraintValidator implements ConstraintValidator<SkillDtoConstraint, SkillDto> {
    private CategoryService categoryService;

    @Override
    public boolean isValid(SkillDto skillDto, ConstraintValidatorContext constraintValidatorContext) {
        if (categoryService.contains(skillDto.getCategoryId())) {
            return true;
        }
        else {
            throw SKILL_NOT_VALID.exception();
        }
    }
}
