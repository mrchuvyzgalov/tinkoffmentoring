package ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.Project;
import ru.mrchuvyzgalov.mentoring.model.dto.MenteeDto;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.service.MenteeService;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.MenteeDtoConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.MENTEE_NOT_VALID;

@AllArgsConstructor
public class MenteeDtoConstraintValidator implements ConstraintValidator<MenteeDtoConstraint, MenteeDto> {
    private ProjectService projectService;

    @Override
    public boolean isValid(MenteeDto menteeDto, ConstraintValidatorContext constraintValidatorContext) {
        if (projectService.contains(menteeDto.getProjectId())) {
            return true;
        }
        else {
            throw MENTEE_NOT_VALID.exception();
        }
    }
}
