package ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint;

import ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint.SkillIdContainsInDbConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SkillIdContainsInDbConstraintValidator.class)
public @interface SkillIdContainsInDbConstraint {
    String message() default "Skill not found";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
