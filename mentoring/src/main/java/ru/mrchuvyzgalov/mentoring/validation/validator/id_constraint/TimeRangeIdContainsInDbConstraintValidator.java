package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.service.TimeRangeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.TimeRangeIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.TIME_RANGE_NOT_FOUND;

@AllArgsConstructor
public class TimeRangeIdContainsInDbConstraintValidator implements ConstraintValidator<TimeRangeIdContainsInDbConstraint, UUID> {
    private TimeRangeService timeRangeService;

    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (timeRangeService.contains(uuid)) {
            return true;
        }
        else {
            throw TIME_RANGE_NOT_FOUND.exception();
        }
    }
}
