package ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorDto;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;
import ru.mrchuvyzgalov.mentoring.service.SkillService;
import ru.mrchuvyzgalov.mentoring.service.TimeRangeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.MentorDtoConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.MENTOR_NOT_VALID;

@AllArgsConstructor
public class MentorDtoConstraintValidator implements ConstraintValidator<MentorDtoConstraint, MentorDto> {
    private ProjectService projectService;
    private SkillService skillService;
    private TimeRangeService timeRangeService;

    @Override
    public boolean isValid(MentorDto mentorDto, ConstraintValidatorContext constraintValidatorContext) {
        if (projectService.contains(mentorDto.getProjectId())) {
            for (UUID skillId : mentorDto.getSkillIds()) {
                if (!skillService.contains(skillId)) {
                    throw MENTOR_NOT_VALID.exception();
                }
            }

            for (UUID timeRangeId : mentorDto.getTimeRangeIds()) {
                if (!timeRangeService.contains(timeRangeId)) {
                    throw MENTOR_NOT_VALID.exception();
                }
            }

            if (mentorDto.getPhoneNumber().matches("^\\d{1}-\\d{3}-\\d{3}-\\d{2}-\\d{2}$")) {
                return true;
            }
            else {
                throw MENTOR_NOT_VALID.exception();
            }
        }
        throw MENTOR_NOT_VALID.exception();
    }
}
