package ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint;

import ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint.MentorIdContainsInDbConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MentorIdContainsInDbConstraintValidator.class)
public @interface MentorIdContainsInDbConstraint {
    String message() default "Mentor not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
