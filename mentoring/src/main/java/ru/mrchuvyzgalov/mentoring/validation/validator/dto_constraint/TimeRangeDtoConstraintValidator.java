package ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint;

import ru.mrchuvyzgalov.mentoring.model.dto.TimeRangeDto;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.TimeRangeDtoConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.TIME_RANGE_NOT_VALID;

public class TimeRangeDtoConstraintValidator implements ConstraintValidator<TimeRangeDtoConstraint, TimeRangeDto> {
    private boolean isCorrectHour(int hour) {
        return hour >= 0 && hour < 24;
    }
    private boolean isCorrectMinute(int minute) {
        return minute >= 0 && minute < 60;
    }
    @Override
    public boolean isValid(TimeRangeDto timeRangeDto, ConstraintValidatorContext constraintValidatorContext) {
        if (!isCorrectHour(timeRangeDto.getStartHour()) || !isCorrectHour(timeRangeDto.getFinishHour())) {
            throw TIME_RANGE_NOT_VALID.exception();
        }

        if (!isCorrectMinute(timeRangeDto.getStartMinutes()) || !isCorrectMinute(timeRangeDto.getFinishMinutes())) {
            throw TIME_RANGE_NOT_VALID.exception();
        }

        if (timeRangeDto.getStartHour() > timeRangeDto.getFinishHour()) {
            throw TIME_RANGE_NOT_VALID.exception();
        }

        if (timeRangeDto.getStartHour() == timeRangeDto.getFinishHour()) {
            if (timeRangeDto.getStartMinutes() > timeRangeDto.getFinishMinutes()) {
                throw TIME_RANGE_NOT_VALID.exception();
            }
        }
        return true;
    }
}
