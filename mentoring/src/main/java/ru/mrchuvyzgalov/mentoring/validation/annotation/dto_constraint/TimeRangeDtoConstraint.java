package ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint;

import ru.mrchuvyzgalov.mentoring.validation.validator.dto_constraint.TimeRangeDtoConstraintValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TimeRangeDtoConstraintValidator.class)
public @interface TimeRangeDtoConstraint {
    String message() default "Time range dto not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
