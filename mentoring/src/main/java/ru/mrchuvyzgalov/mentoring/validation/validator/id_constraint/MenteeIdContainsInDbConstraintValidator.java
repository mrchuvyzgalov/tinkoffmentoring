package ru.mrchuvyzgalov.mentoring.validation.validator.id_constraint;

import lombok.AllArgsConstructor;
import ru.mrchuvyzgalov.mentoring.service.MenteeService;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MenteeIdContainsInDbConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.MENTEE_NOT_FOUND;

@AllArgsConstructor
public class MenteeIdContainsInDbConstraintValidator implements ConstraintValidator<MenteeIdContainsInDbConstraint, UUID> {
    private MenteeService menteeService;

    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext constraintValidatorContext) {
        if (menteeService.contains(uuid)) {
            return true;
        }
        else {
            throw MENTEE_NOT_FOUND.exception();
        }
    }
}
