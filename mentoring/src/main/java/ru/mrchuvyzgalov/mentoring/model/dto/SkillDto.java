package ru.mrchuvyzgalov.mentoring.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.SkillDtoConstraint;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SkillDtoConstraint
public class SkillDto {
    @NotNull
    private String name;
    @NotNull
    private UUID categoryId;
    @NotNull
    private String qualification;
    @NotNull
    private String description;
}
