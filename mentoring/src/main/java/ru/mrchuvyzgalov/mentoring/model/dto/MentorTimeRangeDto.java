package ru.mrchuvyzgalov.mentoring.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mrchuvyzgalov.mentoring.validation.annotation.id_constraint.MentorIdContainsInDbConstraint;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MentorTimeRangeDto {
    @NotNull
    private UUID mentorId;
    @NotNull
    private UUID timeRangeId;
}
