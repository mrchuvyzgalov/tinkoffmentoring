package ru.mrchuvyzgalov.mentoring.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.MentorDtoConstraint;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MentorDtoConstraint
public class MentorDto {
    @NotNull
    private String surname;
    @NotNull
    private String name;
    @NotNull
    private String patronymic;
    @NotNull
    private UUID projectId;
    @NotNull
    private List<@NotNull UUID> skillIds;
    @NotNull
    private String phoneNumber;
    @NotNull
    private Set<@NotNull UUID> timeRangeIds;
}
