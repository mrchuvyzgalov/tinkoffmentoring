package ru.mrchuvyzgalov.mentoring.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mrchuvyzgalov.mentoring.model.TimeRange;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.TimeRangeDtoConstraint;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TimeRangeDtoConstraint
public class TimeRangeDto {
    @NotNull
    private int startHour;
    @NotNull
    private int startMinutes;
    @NotNull
    private int finishHour;
    @NotNull
    private int finishMinutes;
    @NotNull
    private TimeRange.DayOfWeek dayOfWeek;
}
