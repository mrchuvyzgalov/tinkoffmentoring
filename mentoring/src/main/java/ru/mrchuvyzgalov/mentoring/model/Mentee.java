package ru.mrchuvyzgalov.mentoring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Mentee {
    private UUID id;
    private String surname;
    private String name;
    private String patronymic;
    private Project project;
}
