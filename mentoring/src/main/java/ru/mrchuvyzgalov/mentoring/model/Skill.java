package ru.mrchuvyzgalov.mentoring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Skill {
    private UUID id;
    private String name;
    private Category category;
    private String qualification;
    private String description;
}
