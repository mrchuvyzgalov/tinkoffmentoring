package ru.mrchuvyzgalov.mentoring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeRange {
    private UUID id;
    private int startHour;
    private int startMinutes;
    private int finishHour;
    private int finishMinutes;
    private DayOfWeek dayOfWeek;

    public enum DayOfWeek {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
    }
}
