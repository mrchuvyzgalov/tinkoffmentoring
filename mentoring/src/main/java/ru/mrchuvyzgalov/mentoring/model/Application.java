package ru.mrchuvyzgalov.mentoring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.ApplicationDtoConstraint;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Application {
    private UUID id;
    private Mentee mentee;
    private Mentor mentor;
    private TimeRange timeRange;
    private Skill mentorSkill;
    private State state;

    public enum State {
        CREATED, ACCEPTED, REJECTED
    }
}
