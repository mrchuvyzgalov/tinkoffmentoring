package ru.mrchuvyzgalov.mentoring.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mrchuvyzgalov.mentoring.validation.annotation.dto_constraint.ApplicationDtoConstraint;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApplicationDtoConstraint
public class ApplicationDto {
    @NotNull
    private UUID menteeId;
    @NotNull
    private UUID mentorId;
    @NotNull
    private UUID timeRangeId;
    @NotNull
    private UUID mentorSkillId;
}
