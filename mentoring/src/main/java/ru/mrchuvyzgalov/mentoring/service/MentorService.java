package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.mentoring.dao.MentorRepository;
import ru.mrchuvyzgalov.mentoring.model.Mentor;

import java.util.List;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.MENTOR_NOT_FOUND;

@Service
@AllArgsConstructor
public class MentorService {
    private MentorRepository mentorRepository;

    public void save(Mentor mentor) {
        mentorRepository.save(mentor);
    }

    public Mentor findById(UUID id) {
        return mentorRepository.findById(id).get();
    }

    public List<Mentor> findBySkill(UUID id) {
        return mentorRepository.findBySkill(id);
    }

    public List<Mentor> findByCategory(UUID id) {
        return mentorRepository.findByCategory(id);
    }

    public List<Mentor> findAll() {
        return mentorRepository.findAll();
    }

    public void updateById(Mentor mentor) {
        if (!contains(mentor.getId())) {
            throw MENTOR_NOT_FOUND.exception();
        }

        mentorRepository.updateById(mentor);
    }

    public void deleteById(UUID id) {
        if (!contains(id)) {
            throw MENTOR_NOT_FOUND.exception();
        }

        mentorRepository.deleteById(id);
    }

    public void deleteAll() {
        mentorRepository.deleteAll();
    }

    public boolean contains(UUID id) {
        return mentorRepository.findById(id).isPresent();
    }
}
