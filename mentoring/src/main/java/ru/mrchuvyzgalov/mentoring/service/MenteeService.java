package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.mentoring.dao.MenteeRepository;
import ru.mrchuvyzgalov.mentoring.model.Mentee;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class MenteeService {
    private MenteeRepository menteeRepository;

    public void save(Mentee mentee) {
        menteeRepository.save(mentee);
    }

    public Mentee findById(UUID id) {
        return menteeRepository.findById(id).get();
    }

    public List<Mentee> findAll() {
        return menteeRepository.findAll();
    }

    public void updateById(Mentee mentee) {
        menteeRepository.updateById(mentee);
    }

    public void deleteById(UUID id) {
        menteeRepository.deleteById(id);
    }

    public void deleteAll() {
        menteeRepository.deleteAll();
    }

    public boolean contains(UUID id) {
        return menteeRepository.findById(id).isPresent();
    }
}
