package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.mentoring.dao.SkillRepository;
import ru.mrchuvyzgalov.mentoring.model.Skill;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class SkillService {
    private SkillRepository skillRepository;

    public void save(Skill skill) {
        skillRepository.save(skill);
    }

    public Skill findById(UUID id) {
        return skillRepository.findById(id).get();
    }

    public List<Skill> findAll() {
        return skillRepository.findAll();
    }

    public void updateById(Skill skill) {
        skillRepository.updateById(skill);
    }

    public void deleteById(UUID id) {
        skillRepository.deleteById(id);
    }

    public void deleteAll() {
        skillRepository.deleteAll();
    }

    public boolean contains(UUID id) {
        return skillRepository.findById(id).isPresent();
    }
}
