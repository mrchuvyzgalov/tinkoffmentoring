package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.mentoring.dao.CategoryRepository;
import ru.mrchuvyzgalov.mentoring.model.Category;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class CategoryService {
    private CategoryRepository categoryRepository;

    public void save(Category category) {
        categoryRepository.save(category);
    }

    public Category findById(UUID id) {
        return categoryRepository.findById(id).get();
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    public void updateById(Category category) {
        categoryRepository.updateById(category);
    }

    public void deleteById(UUID id) {
        categoryRepository.deleteById(id);
    }

    public void deleteAll() {
        categoryRepository.deleteAll();
    }

    public boolean contains(UUID id) {
        return categoryRepository.findById(id).isPresent();
    }
}
