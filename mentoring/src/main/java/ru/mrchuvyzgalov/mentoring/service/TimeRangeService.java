package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.mentoring.dao.TimeRangeRepository;
import ru.mrchuvyzgalov.mentoring.model.TimeRange;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorTimeRangeDto;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.TIME_RANGE_NOT_FOUND;

@Service
@AllArgsConstructor
public class TimeRangeService {
    private TimeRangeRepository timeRangeRepository;

    public void save(TimeRange timeRange) {
        timeRangeRepository.save(timeRange);
    }

    public TimeRange findById(UUID id) {
        return timeRangeRepository.findById(id).get();
    }

    public Set<TimeRange> findFreeTimeRangeByMentorId(UUID mentorId) {
        return timeRangeRepository.findFreeTimeRangeByMentorId(mentorId);
    }

    public List<TimeRange> findAll() {
        return timeRangeRepository.findAll();
    }

    public void updateById(TimeRange timeRange) {
        timeRangeRepository.updateById(timeRange);
    }

    public void deleteById(UUID id) {
        if (!contains(id)) {
            throw TIME_RANGE_NOT_FOUND.exception();
        }

        timeRangeRepository.deleteById(id);
    }

    public void deleteAll() {
        timeRangeRepository.deleteAll();
    }

    public void bookTimeRangeWithIdAndMentorId(MentorTimeRangeDto mentorTimeRangeDto) {
        TimeRange timeRange = findById(mentorTimeRangeDto.getTimeRangeId());

        Set<TimeRange> timeRanges = findFreeTimeRangeByMentorId(mentorTimeRangeDto.getMentorId());
        if (!timeRanges.contains(timeRange)) {
            throw TIME_RANGE_NOT_FOUND.exception();
        }

        timeRangeRepository.bookTimeRangeWithIdAndMentorId(mentorTimeRangeDto);
    }

    public boolean contains(UUID id) {
        return timeRangeRepository.findById(id).isPresent();
    }
}
