package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.mentoring.dao.ProjectRepository;
import ru.mrchuvyzgalov.mentoring.model.Project;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ProjectService {
    private ProjectRepository projectRepository;

    public void save(Project project) {
        projectRepository.save(project);
    }

    public Project findById(UUID id) {
        return projectRepository.findById(id).get();
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public void updateById(Project project) {
        projectRepository.updateById(project);
    }

    public void deleteById(UUID id) {
        projectRepository.deleteById(id);
    }

    public void deleteAll() {
        projectRepository.deleteAll();
    }

    public boolean contains(UUID id) {
        return projectRepository.findById(id).isPresent();
    }
}
