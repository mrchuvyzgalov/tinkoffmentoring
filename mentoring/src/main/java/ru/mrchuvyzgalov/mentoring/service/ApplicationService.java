package ru.mrchuvyzgalov.mentoring.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mrchuvyzgalov.mentoring.dao.ApplicationRepository;
import ru.mrchuvyzgalov.mentoring.model.Application;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorTimeRangeDto;

import java.util.List;
import java.util.UUID;

import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.APPLICATION_NOT_FOUND;

@Service
@AllArgsConstructor
public class ApplicationService {
    private ApplicationRepository applicationRepository;
    private TimeRangeService timeRangeService;

    @Transactional
    public void acceptApplication(UUID id) {
        Application application = findById(id);
        application.setState(Application.State.ACCEPTED);
        updateById(application);
        timeRangeService.bookTimeRangeWithIdAndMentorId(new MentorTimeRangeDto(application.getMentor().getId(), application.getTimeRange().getId()));
    }

    @Transactional
    public void rejectApplication(UUID id) {
        Application application = findById(id);
        application.setState(Application.State.REJECTED);
        updateById(application);
    }

    public void save(Application application) {
        applicationRepository.save(application);
    }

    public Application findById(UUID id) {
        return applicationRepository.findById(id).get();
    }

    public List<Application> findAll() {
        return applicationRepository.findAll();
    }

    public List<Application> findCreatedApplications() {
        return  applicationRepository.findCreatedApplications();
    }

    public void updateById(Application application) {
        applicationRepository.updateById(application);
    }

    public void deleteById(UUID id) {
        applicationRepository.deleteById(id);
    }

    public void deleteAll() {
        applicationRepository.deleteAll();
    }

    public boolean contains(UUID id) {
        return applicationRepository.findById(id).isPresent();
    }
}
