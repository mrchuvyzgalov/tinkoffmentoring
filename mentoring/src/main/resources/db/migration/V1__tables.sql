CREATE TABLE category
(
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(250) NOT NULL
);

CREATE TABLE skill
(
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    category_id UUID,
    qualification VARCHAR(50) NOT NULL,
    description VARCHAR(250) NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE
);

CREATE TABLE project
(
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(250) NOT NULL
);

CREATE TABLE mentee
(
    id UUID PRIMARY KEY,
    surname VARCHAR(50) NOT NULL,
    name VARCHAR(50) NOT NULL,
    patronymic VARCHAR(50) NOT NULL,
    project_id UUID,
    FOREIGN KEY (project_id) REFERENCES project(id)
);

CREATE TABLE time_range
(
    id UUID PRIMARY KEY,
    start_hour INTEGER NOT NULL,
    start_minutes INTEGER NOT NULL,
    finish_hour INTEGER NOT NULL,
    finish_minutes INTEGER NOT NULL,
    day_of_week VARCHAR(6) NOT NULL
);

CREATE TABLE mentor
(
    id UUID PRIMARY KEY,
    surname VARCHAR(50) NOT NULL,
    name VARCHAR(50) NOT NULL,
    patronymic VARCHAR(50) NOT NULL,
    project_id UUID,
    communication_method VARCHAR(250) NOT NULL,
    FOREIGN KEY (project_id) REFERENCES project(id)
);

CREATE TABLE mentor_skill
(
    mentor_id UUID,
    skill_id UUID,
    FOREIGN KEY (mentor_id) REFERENCES mentor(id) ON DELETE CASCADE,
    FOREIGN KEY (skill_id) REFERENCES skill(id) ON DELETE CASCADE,
    PRIMARY KEY (mentor_id, skill_id)
);

CREATE TABLE mentor_time_range
(
    mentor_id UUID,
    time_range_id UUID,
    is_free BIT,
    FOREIGN KEY (mentor_id) REFERENCES mentor(id) ON DELETE CASCADE,
    FOREIGN KEY (time_range_id) REFERENCES time_range(id) ON DELETE CASCADE,
    PRIMARY KEY (mentor_id, time_range_id)
);

CREATE TABLE application
(
    id UUID PRIMARY KEY,
    mentee_id UUID NOT NULL,
    mentor_id UUID NOT NULL,
    time_range_id UUID NOT NULL,
    skill_id UUID NOT NULL,
    state VARCHAR(10) NOT NULL,
    FOREIGN KEY (mentee_id) REFERENCES mentee(id) ON DELETE CASCADE,
    FOREIGN KEY (mentor_id) REFERENCES mentor(id) ON DELETE CASCADE,
    FOREIGN KEY (time_range_id) REFERENCES time_range(id) ON DELETE CASCADE,
    FOREIGN KEY (skill_id) REFERENCES skill(id) ON DELETE CASCADE
);