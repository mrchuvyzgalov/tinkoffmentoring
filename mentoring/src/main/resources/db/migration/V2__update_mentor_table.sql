ALTER TABLE mentor
    DROP COLUMN communication_method;

ALTER TABLE mentor
    ADD COLUMN phone_number VARCHAR(16) NOT NULL;