package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.*;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorTimeRangeDto;
import ru.mrchuvyzgalov.mentoring.model.dto.TimeRangeDto;
import ru.mrchuvyzgalov.mentoring.service.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.*;

@AutoConfigureMockMvc
public class TimeRangeControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TimeRangeService timeRangeService;
    @Autowired
    private MentorService mentorService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private ProjectService projectService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewTimeRangeAdminSuccess() {
        TimeRangeDto timeRangeDto = new TimeRangeDto(12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        mockMvc.perform(post("/time-range/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRangeDto)))
                .andExpect(status().isOk());

        List<TimeRange> dbEntity = timeRangeService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getStartHour(), timeRangeDto.getStartHour());
        assertEquals(dbEntity.get(0).getStartMinutes(), timeRangeDto.getStartMinutes());
        assertEquals(dbEntity.get(0).getFinishHour(), timeRangeDto.getFinishHour());
        assertEquals(dbEntity.get(0).getFinishMinutes(), timeRangeDto.getFinishMinutes());
        assertEquals(dbEntity.get(0).getDayOfWeek(), timeRangeDto.getDayOfWeek());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewNotValidTimeRangeAdminFailure() {
        TimeRangeDto timeRangeDto = new TimeRangeDto(15, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        mockMvc.perform(post("/time-range/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRangeDto)))
                .andExpect(status().is(TIME_RANGE_NOT_VALID.getCode()))
                .andExpect(jsonPath("code").value(TIME_RANGE_NOT_VALID.getCode()))
                .andExpect(jsonPath("message").value(TIME_RANGE_NOT_VALID.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewNotValidTimeRangeUserFailure() {
        TimeRangeDto timeRangeDto = new TimeRangeDto(15, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(post("/time-range/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRangeDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewTimeRangeUserFailure() {
        TimeRangeDto timeRangeDto = new TimeRangeDto(15, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(post("/time-range/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRangeDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateTimeRangeAdminSuccess() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange);

        timeRange.setDayOfWeek(TimeRange.DayOfWeek.MONDAY);

        TimeRangeDto timeRangeDto = new TimeRangeDto(12, 0, 14, 0, TimeRange.DayOfWeek.MONDAY);
        mockMvc.perform(put("/time-range/update")
                        .param("id", timeRange.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRangeDto)))
                .andExpect(status().isOk());

        TimeRange dbEntity = timeRangeService.findById(timeRange.getId());
        assertEquals(timeRange, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateTimeRangeAdminFailure() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(put("/time-range/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRange)))
                .andExpect(status().is(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(TIME_RANGE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateTimeRangeUserFailure() {
        TimeRangeDto timeRangeDto = new TimeRangeDto(15, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(put("/time-range/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(timeRangeDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteTimeRangeAdminSuccess() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange);

        mockMvc.perform(delete("/time-range/delete")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(timeRangeService.contains(timeRange.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteTimeRangeAdminFailure() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(delete("/time-range/delete")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().is(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(TIME_RANGE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteTimeRangeUserFailure() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(delete("/time-range/delete")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllTimeRangeAdminSuccess() {
        TimeRange timeRange1 = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange1);

        TimeRange timeRange2 = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange2);

        mockMvc.perform(delete("/time-range/delete-all"))
                .andExpect(status().isOk());

        assertEquals(timeRangeService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllTimeRangeUserFailure() {
        TimeRange timeRange1 = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange1);

        TimeRange timeRange2 = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange2);

        mockMvc.perform(delete("/time-range/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetTimeRangeAdminSuccess() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange);

        mockMvc.perform(get("/time-range/get")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(timeRange)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetTimeRangeUserSuccess() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange);

        mockMvc.perform(get("/time-range/get")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(timeRange)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetFreeTimeRangesByMentorIdAdminSuccess() {
        Mentor mentor = createMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/time-range/get-free-time-range")
                        .param("mentorId", mentor.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(mentor.getTimeRange())));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetFreeTimeRangesByMentorIdUserSuccess() {
        Mentor mentor = createMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/time-range/get-free-time-range")
                        .param("mentorId", mentor.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(mentor.getTimeRange())));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetFreeTimeRangesByMentorIdAdminFailure() {
        mockMvc.perform(get("/time-range/get-free-time-range")
                        .param("mentorId", UUID.randomUUID().toString()))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTOR_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetFreeTimeRangesByMentorIdUserFailure() {
        mockMvc.perform(get("/time-range/get-free-time-range")
                        .param("mentorId", UUID.randomUUID().toString()))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTOR_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetTimeRangeAdminFailure() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(get("/time-range/get")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().is(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(TIME_RANGE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetTimeRangeUserFailure() {
        TimeRange timeRange = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);

        mockMvc.perform(get("/time-range/get")
                        .param("id", timeRange.getId().toString()))
                .andExpect(status().is(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(TIME_RANGE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(TIME_RANGE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllTimeRangeAdminSuccess() {
        TimeRange timeRange1 = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange1);

        mockMvc.perform(get("/time-range/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(timeRange1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllTimeRangeUserSuccess() {
        TimeRange timeRange1 = new TimeRange(UUID.randomUUID(), 12, 0, 14, 0, TimeRange.DayOfWeek.FRIDAY);
        timeRangeService.save(timeRange1);

        mockMvc.perform(get("/time-range/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(timeRange1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void bookTimeRangeWithIdAndMentorIdAdminSuccess() {
        Mentor mentor = createMentor();

        saveMentorToDb(mentor);

        MentorTimeRangeDto mentorTimeRange = new MentorTimeRangeDto(mentor.getId(), mentor.getTimeRange().iterator().next().getId());

        mockMvc.perform(put("/time-range/book-time-range")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorTimeRange)))
                .andExpect(status().isOk());

        Set<TimeRange> timeRanges = timeRangeService.findFreeTimeRangeByMentorId(mentor.getId());

        assertTrue(timeRanges.isEmpty());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void bookTimeRangeWithIdAndMentorIdAdminFailure() {
        Mentor mentor = createMentor();

        saveMentorContentToDb(mentor);

        MentorTimeRangeDto mentorTimeRange = new MentorTimeRangeDto(mentor.getId(), mentor.getTimeRange().iterator().next().getId());

        mockMvc.perform(put("/time-range/book-time-range")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorTimeRange)))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void bookTimeRangeWithIdAndMentorIdUserFailure() {
        Mentor mentor = createMentor();

        saveMentorToDb(mentor);

        MentorTimeRangeDto mentorTimeRange = new MentorTimeRangeDto(mentor.getId(), mentor.getTimeRange().iterator().next().getId());

        mockMvc.perform(put("/time-range/book-time-range")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorTimeRange)))
                .andExpect(status().is(403));
    }

    private Mentor createMentor() {
        Project project = new Project(UUID.randomUUID(), "First project name", "First project description");
        Category category = new Category(UUID.randomUUID(), "First category name", "First category description");
        List<Skill> skills = List.of(
                new Skill(UUID.randomUUID(), "First skill name", category, "First skill qualification", "First skill description")
        );
        Set<TimeRange> timeRanges = Set.of(
                new TimeRange(UUID.randomUUID(), 5, 0, 6, 0, TimeRange.DayOfWeek.FRIDAY)
        );
        return new Mentor(UUID.randomUUID(), "First mentor surname", "First mentor name", "First mentor patronymic", project, skills, "8-912-321-79-11", timeRanges);
    }

    private void saveMentorToDb(Mentor mentor) {
        saveMentorContentToDb(mentor);
        mentorService.save(mentor);
    }

    private void saveMentorContentToDb(Mentor mentor) {
        for (Skill skill : mentor.getSkills()) {
            categoryService.save(skill.getCategory());
            skillService.save(skill);
        }

        projectService.save(mentor.getProject());

        for (TimeRange timeRange : mentor.getTimeRange()) {
            timeRangeService.save(timeRange);
        }
    }
}
