package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.*;
import ru.mrchuvyzgalov.mentoring.model.dto.MentorDto;
import ru.mrchuvyzgalov.mentoring.service.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.*;

@AutoConfigureMockMvc
public class MentorControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TimeRangeService timeRangeService;
    @Autowired
    private MentorService mentorService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewMentorAdminSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorContentToDb(mentor);

        MentorDto mentorDto = getMentorDto(mentor);

        mockMvc.perform(post("/mentor/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorDto)))
                .andExpect(status().isOk());

        List<Mentor> dbEntity = mentorService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getSurname(), mentor.getSurname());
        assertEquals(dbEntity.get(0).getName(), mentor.getName());
        assertEquals(dbEntity.get(0).getPatronymic(), mentor.getPatronymic());
        assertEquals(dbEntity.get(0).getProject(), mentor.getProject());
        assertEquals(dbEntity.get(0).getSkills(), mentor.getSkills());
        assertEquals(dbEntity.get(0).getTimeRange(), mentor.getTimeRange());
        assertEquals(dbEntity.get(0).getPhoneNumber(), mentor.getPhoneNumber());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewMentorUserFailure() {
        Mentor mentor = createFirstMentor();
        saveMentorContentToDb(mentor);
        MentorDto mentorDto = getMentorDto(mentor);

        mockMvc.perform(post("/mentor/create")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateMentorAdminSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mentor.setSurname("New surname project");
        MentorDto mentorDto = getMentorDto(mentor);

        mockMvc.perform(put("/mentor/update")
                        .param("id", mentor.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorDto)))
                .andExpect(status().isOk());

        Mentor dbEntity = mentorService.findById(mentor.getId());
        assertEquals(mentor, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateMentorAdminFailure() {
        Mentor mentor = createFirstMentor();
        saveMentorContentToDb(mentor);
        MentorDto mentorDto = getMentorDto(mentor);

        mockMvc.perform(put("/mentor/update")
                        .param("id", mentor.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentorDto)))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTOR_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateMentorUserFailure() {
        Mentor mentor = createFirstMentor();

        mockMvc.perform(put("/mentor/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentor)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteMentorAdminSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(delete("/mentor/delete")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(mentorService.contains(mentor.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteMentorAdminFailure() {
        Mentor mentor = createFirstMentor();
        saveMentorContentToDb(mentor);

        mockMvc.perform(delete("/mentor/delete")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTOR_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteMentorUserFailure() {
        Mentor mentor = createFirstMentor();
        saveMentorContentToDb(mentor);

        mockMvc.perform(delete("/mentor/delete")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllMentorsAdminSuccess() {
        Mentor mentor1 = createFirstMentor();
        Mentor mentor2 = createSecondMentor();

        saveMentorToDb(mentor1);
        saveMentorToDb(mentor2);

        mockMvc.perform(delete("/mentor/delete-all"))
                .andExpect(status().isOk());

        assertEquals(mentorService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllMentorsUserFailure() {
        Mentor mentor1 = createFirstMentor();
        Mentor mentor2 = createSecondMentor();

        saveMentorToDb(mentor1);
        saveMentorToDb(mentor2);

        mockMvc.perform(delete("/mentor/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetMentorAdminSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/mentor/get")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(mentor)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetMentorUserSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/mentor/get")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(mentor)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetMentorBySkillAdminSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/mentor/get-by-skill")
                        .param("id", mentor.getSkills().get(0).getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentor))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetMentorBySkillUserSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/mentor/get-by-skill")
                        .param("id", mentor.getSkills().get(0).getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentor))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetMentorByCategoryAdminSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/mentor/get-by-category")
                        .param("id", mentor.getSkills().get(0).getCategory().getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentor))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetMentorByCategoryUserSuccess() {
        Mentor mentor = createFirstMentor();
        saveMentorToDb(mentor);

        mockMvc.perform(get("/mentor/get-by-category")
                        .param("id", mentor.getSkills().get(0).getCategory().getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentor))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetMentorAdminFailure() {
        Mentor mentor = createFirstMentor();

        mockMvc.perform(get("/mentor/get")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTOR_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetMentorUserFailure() {
        Mentor mentor = createFirstMentor();

        mockMvc.perform(get("/mentor/get")
                        .param("id", mentor.getId().toString()))
                .andExpect(status().is(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTOR_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTOR_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllMentorsAdminSuccess() {
        Mentor mentor1 = createFirstMentor();

        saveMentorToDb(mentor1);

        mockMvc.perform(get("/mentor/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentor1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllMentorsUserSuccess() {
        Mentor mentor1 = createFirstMentor();

        saveMentorToDb(mentor1);

        mockMvc.perform(get("/mentor/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentor1))));
    }

    private Mentor createFirstMentor() {
        Project project = new Project(UUID.randomUUID(), "First project name", "First project description");
        Category category = new Category(UUID.randomUUID(), "First category name", "First category description");
        List<Skill> skills = List.of(
                new Skill(UUID.randomUUID(), "First skill name", category, "First skill qualification", "First skill description")
        );
        Set<TimeRange> timeRanges = Set.of(
                new TimeRange(UUID.randomUUID(), 5, 0, 6, 0, TimeRange.DayOfWeek.FRIDAY)
        );
        return new Mentor(UUID.randomUUID(), "First mentor surname", "First mentor name", "First mentor patronymic", project, skills, "8-123-123-11-11", timeRanges);
    }

    private Mentor createSecondMentor() {
        Project project = new Project(UUID.randomUUID(), "Second project name", "Second project description");
        Category category = new Category(UUID.randomUUID(), "Second category name", "Second category description");
        List<Skill> skills = List.of(
                new Skill(UUID.randomUUID(), "Second skill name", category, "Second skill qualification", "Second skill description")
        );
        Set<TimeRange> timeRanges = Set.of(
                new TimeRange(UUID.randomUUID(), 10, 0, 15, 0, TimeRange.DayOfWeek.FRIDAY)
        );
        return new Mentor(UUID.randomUUID(), "Second mentor surname", "Second mentor name", "Second mentor patronymic", project, skills, "8-123-123-12-11", timeRanges);
    }

    private void saveMentorToDb(Mentor mentor) {
        saveMentorContentToDb(mentor);
        mentorService.save(mentor);
    }

    private void saveMentorContentToDb(Mentor mentor) {
        for (Skill skill : mentor.getSkills()) {
            categoryService.save(skill.getCategory());
            skillService.save(skill);
        }

        projectService.save(mentor.getProject());

        for (TimeRange timeRange : mentor.getTimeRange()) {
            timeRangeService.save(timeRange);
        }
    }

    private MentorDto getMentorDto(Mentor mentor) {
        List<UUID> skillIds = new ArrayList<>();
        for (Skill skill : mentor.getSkills()) {
            skillIds.add(skill.getId());
        }

        Set<UUID> timeRangeIds = new HashSet<>();
        for (TimeRange timeRange : mentor.getTimeRange()) {
            timeRangeIds.add(timeRange.getId());
        }

        MentorDto mentorDto = new MentorDto(mentor.getSurname(), mentor.getName(), mentor.getPatronymic(),
                mentor.getProject().getId(), skillIds, mentor.getPhoneNumber(), timeRangeIds);

        return mentorDto;
    }
}
