package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.Project;
import ru.mrchuvyzgalov.mentoring.model.dto.ProjectDto;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.PROJECT_IS_EXIST;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.PROJECT_NOT_FOUND;

@AutoConfigureMockMvc
public class ProjectControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProjectService projectService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewProjectAdminSuccess() {
        ProjectDto projectDto = new ProjectDto("Name", "Description");
        mockMvc.perform(post("/project/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(projectDto)))
                .andExpect(status().isOk());

        List<Project> dbEntity = projectService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getName(), projectDto.getName());
        assertEquals(dbEntity.get(0).getDescription(), projectDto.getDescription());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewProjectUserFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(post("/project/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(project)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateProjectAdminSuccess() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");
        projectService.save(project);
        project.setDescription("New description");

        ProjectDto projectDto = new ProjectDto("Name", "New description");

        mockMvc.perform(put("/project/update")
                        .param("id", project.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(projectDto)))
                .andExpect(status().isOk());

        Project dbEntity = projectService.findById(project.getId());
        assertEquals(project, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateProjectAdminFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(put("/project/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(project)))
                .andExpect(status().is(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(PROJECT_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateProjectUserFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(put("/project/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(project)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteProjectAdminSuccess() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        projectService.save(project);

        mockMvc.perform(delete("/project/delete")
                        .param("id", project.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(projectService.contains(project.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteProjectAdminFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(delete("/project/delete")
                        .param("id", project.getId().toString()))
                .andExpect(status().is(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(PROJECT_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteProjectUserFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(delete("/project/delete")
                        .param("id", project.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllProjectsAdminSuccess() {
        Project project1 = new Project(UUID.randomUUID(), "Name 1", "Description 1");
        projectService.save(project1);

        Project project2 = new Project(UUID.randomUUID(), "Name 2", "Description 2");
        projectService.save(project2);

        mockMvc.perform(delete("/project/delete-all"))
                .andExpect(status().isOk());

        assertEquals(projectService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllProjectsUserFailure() {
        Project project1 = new Project(UUID.randomUUID(), "Name 1", "Description 1");
        projectService.save(project1);

        Project project2 = new Project(UUID.randomUUID(), "Name 2", "Description 2");
        projectService.save(project2);

        mockMvc.perform(delete("/project/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetProjectAdminSuccess() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");
        projectService.save(project);

        mockMvc.perform(get("/project/get")
                        .param("id", project.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(project)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetProjectUserSuccess() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");
        projectService.save(project);

        mockMvc.perform(get("/project/get")
                        .param("id", project.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(project)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetProjectAdminFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(get("/project/get")
                        .param("id", project.getId().toString()))
                .andExpect(status().is(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(PROJECT_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetProjectUserFailure() {
        Project project = new Project(UUID.randomUUID(), "Name", "Description");

        mockMvc.perform(get("/project/get")
                        .param("id", project.getId().toString()))
                .andExpect(status().is(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(PROJECT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(PROJECT_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllProjectsAdminSuccess() {
        Project project1 = new Project(UUID.randomUUID(), "Name 1", "Description 1");
        projectService.save(project1);

        mockMvc.perform(get("/project/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(project1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllProjectsUserSuccess() {
        Project project1 = new Project(UUID.randomUUID(), "Name 1", "Description 1");
        projectService.save(project1);


        mockMvc.perform(get("/project/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(project1))));
    }
}
