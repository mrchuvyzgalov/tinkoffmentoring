package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.Category;
import ru.mrchuvyzgalov.mentoring.model.Skill;
import ru.mrchuvyzgalov.mentoring.model.dto.SkillDto;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;
import ru.mrchuvyzgalov.mentoring.service.SkillService;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.SKILL_NOT_FOUND;

@AutoConfigureMockMvc
public class SkillControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SkillService skillService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewSkillAdminSuccess() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());

        SkillDto skillDto = new SkillDto(skill.getName(), skill.getCategory().getId(), skill.getQualification(), skill.getDescription());
        mockMvc.perform(post("/skill/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(skillDto)))
                .andExpect(status().isOk());


        List<Skill> dbEntity = skillService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getName(), skillDto.getName());
        assertEquals(dbEntity.get(0).getCategory(), skill.getCategory());
        assertEquals(dbEntity.get(0).getQualification(), skillDto.getQualification());
        assertEquals(dbEntity.get(0).getDescription(), skillDto.getDescription());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "User")
    void testAddNewSkillUserFailure() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());

        SkillDto skillDto = new SkillDto(skill.getName(), skill.getCategory().getId(), skill.getQualification(), skill.getDescription());

        mockMvc.perform(post("/skill/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(skillDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateSkillAdminSuccess() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());
        skillService.save(skill);

        skill.setDescription("New Description");
        SkillDto skillDto = new SkillDto(skill.getName(), skill.getCategory().getId(), skill.getQualification(), skill.getDescription());

        mockMvc.perform(put("/skill/update")
                        .param("id", skill.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(skillDto)))
                .andExpect(status().isOk());

        Skill dbEntity = skillService.findById(skill.getId());
        assertEquals(skill, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateSkillAdminFailure() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());
        SkillDto skillDto = new SkillDto(skill.getName(), skill.getCategory().getId(), skill.getQualification(), skill.getDescription());

        mockMvc.perform(put("/skill/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(skillDto)))
                .andExpect(status().is(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(SKILL_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateSkillUserFailure() {
        Skill skill = createFirstSkill();

        mockMvc.perform(put("/skill/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(skill)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteSkillAdminSuccess() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());
        skillService.save(skill);

        mockMvc.perform(delete("/skill/delete")
                        .param("id", skill.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(skillService.contains(skill.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteSkillAdminFailure() {
        Skill skill = createFirstSkill();

        categoryService.save(skill.getCategory());

        mockMvc.perform(delete("/skill/delete")
                        .param("id", skill.getId().toString()))
                .andExpect(status().is(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(SKILL_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteSkillUserFailure() {
        Skill skill = createFirstSkill();

        categoryService.save(skill.getCategory());

        mockMvc.perform(delete("/skill/delete")
                        .param("id", skill.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllSkillsAdminSuccess() {
        Skill skill1 = createFirstSkill();
        Skill skill2 = createSecondSkill();

        categoryService.save(skill1.getCategory());
        categoryService.save(skill2.getCategory());

        skillService.save(skill1);
        skillService.save(skill2);

        mockMvc.perform(delete("/skill/delete-all"))
                .andExpect(status().isOk());

        assertEquals(skillService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllSkillsUserFailure() {
        Skill skill1 = createFirstSkill();
        Skill skill2 = createSecondSkill();

        categoryService.save(skill1.getCategory());
        categoryService.save(skill2.getCategory());

        skillService.save(skill1);
        skillService.save(skill2);

        mockMvc.perform(delete("/skill/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetSkillAdminSuccess() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());
        skillService.save(skill);
        mockMvc.perform(get("/skill/get")
                        .param("id", skill.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(skill)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetSkillUserSuccess() {
        Skill skill = createFirstSkill();
        categoryService.save(skill.getCategory());
        skillService.save(skill);
        mockMvc.perform(get("/skill/get")
                        .param("id", skill.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(skill)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetSkillAdminFailure() {
        Skill skill = createFirstSkill();

        mockMvc.perform(get("/skill/get")
                        .param("id", skill.getId().toString()))
                .andExpect(status().is(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(SKILL_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetSkillUserFailure() {
        Skill skill = createFirstSkill();

        mockMvc.perform(get("/skill/get")
                        .param("id", skill.getId().toString()))
                .andExpect(status().is(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(SKILL_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(SKILL_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllSkillsAdminSuccess() {
        Skill skill1 = createFirstSkill();

        categoryService.save(skill1.getCategory());

        skillService.save(skill1);

        mockMvc.perform(get("/skill/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(skill1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllSkillsUserSuccess() {
        Skill skill1 = createFirstSkill();

        categoryService.save(skill1.getCategory());

        skillService.save(skill1);

        mockMvc.perform(get("/skill/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(skill1))));
    }

    private Skill createFirstSkill() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        return new Skill(UUID.randomUUID(), "First Name", category, "First Qualification", "First Description");
    }

    private Skill createSecondSkill() {
        Category category = new Category(UUID.randomUUID(), "Math", "Linear algebra");
        return new Skill(UUID.randomUUID(), "Second Name", category, "Second Qualification", "Second Description");
    }
}
