package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.*;
import ru.mrchuvyzgalov.mentoring.model.dto.ApplicationDto;
import ru.mrchuvyzgalov.mentoring.service.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.*;

@AutoConfigureMockMvc
public class ApplicationControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TimeRangeService timeRangeService;
    @Autowired
    private MentorService mentorService;
    @Autowired
    private MenteeService menteeService;
    @Autowired
    private ApplicationService applicationService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewApplicationAdminSuccess() {
        Application application = createFirstApplication();
        saveApplicationContentToDb(application);

        ApplicationDto applicationDto = new ApplicationDto(application.getMentee().getId(), application.getMentor().getId(),
                application.getTimeRange().getId(), application.getMentorSkill().getId());

        mockMvc.perform(post("/application/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(applicationDto)))
                .andExpect(status().isOk());

        List<Application> dbEntity = applicationService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getMentee(), application.getMentee());
        assertEquals(dbEntity.get(0).getMentor(), application.getMentor());
        assertEquals(dbEntity.get(0).getTimeRange(), application.getTimeRange());
        assertEquals(dbEntity.get(0).getMentorSkill(), application.getMentorSkill());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewApplicationUserSuccess() {
        Application application = createFirstApplication();
        saveApplicationContentToDb(application);

        ApplicationDto applicationDto = new ApplicationDto(application.getMentee().getId(), application.getMentor().getId(),
                application.getTimeRange().getId(), application.getMentorSkill().getId());

        mockMvc.perform(post("/application/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(applicationDto)))
                .andExpect(status().isOk());

        List<Application> dbEntity = applicationService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getMentee(), application.getMentee());
        assertEquals(dbEntity.get(0).getMentor(), application.getMentor());
        assertEquals(dbEntity.get(0).getTimeRange(), application.getTimeRange());
        assertEquals(dbEntity.get(0).getMentorSkill(), application.getMentorSkill());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAcceptApplicationAdminSuccess() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(put("/application/accept")
                .param("id", application.getId().toString()))
                .andExpect(status().isOk());

        application.setState(Application.State.ACCEPTED);
        Application dbEntity = applicationService.findById(application.getId());
        assertEquals(application, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAcceptApplicationAdminFailure() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        applicationService.rejectApplication(application.getId());

        mockMvc.perform(put("/application/accept")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(APPLICATION_NOT_VALID.getCode()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAcceptApplicationUserFailure() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(put("/application/accept")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testRejectApplicationAdminSuccess() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(put("/application/reject")
                        .param("id", application.getId().toString()))
                .andExpect(status().isOk());

        application.setState(Application.State.REJECTED);
        Application dbEntity = applicationService.findById(application.getId());
        assertEquals(application, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testRejectApplicationAdminFailure() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        applicationService.rejectApplication(application.getId());

        mockMvc.perform(put("/application/reject")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(APPLICATION_NOT_VALID.getCode()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testRejectApplicationUserFailure() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(put("/application/reject")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateApplicationAdminSuccess() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        Project project = new Project(UUID.randomUUID(), "Project name 2", "Project description 2");
        Mentee mentee = new Mentee(UUID.randomUUID(), "Mentee surname 2", "Mentee name 2", "Mentee patronymic 2", project);
        projectService.save(project);
        menteeService.save(mentee);

        application.setMentee(mentee);

        ApplicationDto applicationDto = new ApplicationDto(application.getMentee().getId(), application.getMentor().getId(),
                application.getTimeRange().getId(), application.getMentorSkill().getId());

        mockMvc.perform(put("/application/update")
                        .param("id", application.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(applicationDto)))
                .andExpect(status().isOk());

        Application dbEntity = applicationService.findById(application.getId());
        assertEquals(application, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateApplicationAdminFailure() {
        Application application = createFirstApplication();
        saveApplicationContentToDb(application);

        ApplicationDto applicationDto = new ApplicationDto(application.getMentee().getId(), application.getMentor().getId(),
                application.getTimeRange().getId(), application.getMentorSkill().getId());

        mockMvc.perform(put("/application/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(applicationDto)))
                .andExpect(status().is(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(APPLICATION_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateApplicationUserFailure() {
        Application application = createFirstApplication();

        mockMvc.perform(put("/application/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(application)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteApplicationAdminSuccess() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(delete("/application/delete")
                        .param("id", application.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(applicationService.contains(application.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteApplicationAdminFailure() {
        Application application = createFirstApplication();
        saveApplicationContentToDb(application);

        mockMvc.perform(delete("/application/delete")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(APPLICATION_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteApplicationUserFailure() {
        Application application = createFirstApplication();
        saveApplicationContentToDb(application);

        mockMvc.perform(delete("/application/delete")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllApplicationsAdminSuccess() {
        Application application1 = createFirstApplication();
        Application application2 = createSecondApplication();

        saveApplicationToDb(application1);
        saveApplicationToDb(application2);

        mockMvc.perform(delete("/application/delete-all"))
                .andExpect(status().isOk());

        assertEquals(applicationService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllApplicationsUserFailure() {
        Application application1 = createFirstApplication();
        Application application2 = createSecondApplication();

        saveApplicationToDb(application1);
        saveApplicationToDb(application2);

        mockMvc.perform(delete("/application/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetApplicationAdminSuccess() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(get("/application/get")
                        .param("id", application.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(application)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetApplicationUserSuccess() {
        Application application = createFirstApplication();
        saveApplicationToDb(application);

        mockMvc.perform(get("/application/get")
                        .param("id", application.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(application)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetApplicationAdminFailure() {
        Application application = createFirstApplication();

        mockMvc.perform(get("/application/get")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(APPLICATION_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetApplicationUserFailure() {
        Application application = createFirstApplication();

        mockMvc.perform(get("/application/get")
                        .param("id", application.getId().toString()))
                .andExpect(status().is(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(APPLICATION_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(APPLICATION_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllApplicationsAdminSuccess() {
        Application application1 = createFirstApplication();

        saveApplicationToDb(application1);

        mockMvc.perform(get("/application/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(application1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllApplicationsUserSuccess() {
        Application application1 = createFirstApplication();

        saveApplicationToDb(application1);

        mockMvc.perform(get("/application/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(application1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllCreatedApplicationsUserSuccess() {
        Application application1 = createFirstApplication();

        saveApplicationToDb(application1);

        mockMvc.perform(get("/application/get-created"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(application1))));
    }


    private Application createFirstApplication() {
        Project project = new Project(UUID.randomUUID(), "First project name", "First project description");
        Category category = new Category(UUID.randomUUID(), "First category name", "First category description");
        List<Skill> skills = List.of(
                new Skill(UUID.randomUUID(), "First skill name", category, "First skill qualification", "First skill description")
        );
        Set<TimeRange> timeRanges = Set.of(
                new TimeRange(UUID.randomUUID(), 5, 0, 6, 0, TimeRange.DayOfWeek.FRIDAY)
        );

        Mentor mentor = new Mentor(UUID.randomUUID(), "First mentor surname", "First mentor name", "First mentor patronymic", project, skills, "8-123-678-12-77", timeRanges);
        Mentee mentee = new Mentee(UUID.randomUUID(), "First mentee surname", "First mentee name", "First mentee patronymic", project);

        return new Application(UUID.randomUUID(), mentee, mentor, timeRanges.iterator().next(), skills.get(0), Application.State.CREATED);
    }

    private Application createSecondApplication() {
        Project project = new Project(UUID.randomUUID(), "Second project name", "Second project description");
        Category category = new Category(UUID.randomUUID(), "Second category name", "Second category description");
        List<Skill> skills = List.of(
                new Skill(UUID.randomUUID(), "Second skill name", category, "Second skill qualification", "Second skill description")
        );
        Set<TimeRange> timeRanges = Set.of(
                new TimeRange(UUID.randomUUID(), 10, 0, 15, 0, TimeRange.DayOfWeek.FRIDAY)
        );
        Mentor mentor = new Mentor(UUID.randomUUID(), "Second mentor surname", "Second mentor name", "Second mentor patronymic", project, skills, "5-123-678-12-77", timeRanges);
        Mentee mentee = new Mentee(UUID.randomUUID(), "Second mentee surname", "Second mentee name", "Second mentee patronymic", project);

        return new Application(UUID.randomUUID(), mentee, mentor, timeRanges.iterator().next(), skills.get(0), Application.State.CREATED);
    }

    private void saveApplicationToDb(Application application) {
        saveApplicationContentToDb(application);
        applicationService.save(application);
    }

    private void saveApplicationContentToDb(Application application) {
        Mentor mentor = application.getMentor();

        for (Skill skill : mentor.getSkills()) {
            categoryService.save(skill.getCategory());
            skillService.save(skill);
        }

        projectService.save(mentor.getProject());

        for (TimeRange timeRange : mentor.getTimeRange()) {
            timeRangeService.save(timeRange);
        }

        mentorService.save(mentor);
        menteeService.save(application.getMentee());
    }
}
