package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.Category;
import ru.mrchuvyzgalov.mentoring.model.dto.CategoryDto;
import ru.mrchuvyzgalov.mentoring.service.CategoryService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.*;

import java.util.List;
import java.util.UUID;

@AutoConfigureMockMvc
public class CategoryControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CategoryService categoryService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewCategoryAdminSuccess() {
        CategoryDto categoryDto = new CategoryDto("Programming", "Programming languages");
        mockMvc.perform(post("/category/create")
                .contentType("application/json")
                .content(convertObjectToJsonBytes(categoryDto)))
                .andExpect(status().isOk());

        List<Category> dbEntity = categoryService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getName(), categoryDto.getName());
        assertEquals(dbEntity.get(0).getDescription(), categoryDto.getDescription());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewCategoryUserFailure() {
        CategoryDto categoryDto = new CategoryDto("Programming", "Programming languages");

        mockMvc.perform(post("/category/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(categoryDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateCategoryAdminSuccess() {
        CategoryDto categoryDto = new CategoryDto("Programming", "Functional programming languages");
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category);

        category.setDescription("Functional programming languages");
        mockMvc.perform(put("/category/update")
                        .param("id", category.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(categoryDto)))
                .andExpect(status().isOk());

        Category dbEntity = categoryService.findById(category.getId());
        assertEquals(category, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateCategoryAdminFailure() {
        Category new_category = new Category(UUID.randomUUID(), "Math", "Linear algebra");
        mockMvc.perform(put("/category/update")
                        .param("id", UUID.randomUUID().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(new_category)))
                .andExpect(status().is(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(CATEGORY_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateCategoryUserFailure() {
        CategoryDto categoryDto = new CategoryDto("Programming", "Programming languages");
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category);

        mockMvc.perform(put("/category/update")
                        .param("id", category.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(categoryDto)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteCategoryAdminSuccess() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category);
        mockMvc.perform(delete("/category/delete")
                        .param("id", category.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(categoryService.contains(category.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteCategoryAdminFailure() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");

        mockMvc.perform(delete("/category/delete")
                        .param("id", category.getId().toString()))
                .andExpect(status().is(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(CATEGORY_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteCategoryUserFailure() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");

        mockMvc.perform(delete("/category/delete")
                        .param("id", category.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllCategoriesAdminSuccess() {
        Category category1 = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category1);

        Category category2 = new Category(UUID.randomUUID(), "Math", "Linear algebra");
        categoryService.save(category2);

        mockMvc.perform(delete("/category/delete-all"))
                .andExpect(status().isOk());

        assertEquals(categoryService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllCategoriesUserFailure() {
        Category category1 = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category1);

        Category category2 = new Category(UUID.randomUUID(), "Math", "Linear algebra");
        categoryService.save(category2);

        mockMvc.perform(delete("/category/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetCategoryAdminSuccess() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category);

        mockMvc.perform(get("/category/get")
                        .param("id", category.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(category)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetCategoryUserSuccess() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category);

        mockMvc.perform(get("/category/get")
                        .param("id", category.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(category)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetCategoryAdminFailure() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        mockMvc.perform(get("/category/get")
                        .param("id", category.getId().toString()))
                .andExpect(status().is(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(CATEGORY_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetCategoryUserFailure() {
        Category category = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        mockMvc.perform(get("/category/get")
                        .param("id", category.getId().toString()))
                .andExpect(status().is(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(CATEGORY_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(CATEGORY_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllCategoriesAdminSuccess() {
        Category category1 = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category1);

        mockMvc.perform(get("/category/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(category1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllCategoriesUserSuccess() {
        Category category1 = new Category(UUID.randomUUID(), "Programming", "Programming languages");
        categoryService.save(category1);

        mockMvc.perform(get("/category/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(category1))));
    }
}
