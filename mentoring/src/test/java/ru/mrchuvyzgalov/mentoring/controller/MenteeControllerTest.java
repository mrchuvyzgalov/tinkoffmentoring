package ru.mrchuvyzgalov.mentoring.controller;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.mentoring.AbstractTest;
import ru.mrchuvyzgalov.mentoring.model.Mentee;
import ru.mrchuvyzgalov.mentoring.model.Project;
import ru.mrchuvyzgalov.mentoring.model.dto.MenteeDto;
import ru.mrchuvyzgalov.mentoring.service.MenteeService;
import ru.mrchuvyzgalov.mentoring.service.ProjectService;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.mentoring.exception.ApplicationExceptions.*;

@AutoConfigureMockMvc
public class MenteeControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private MenteeService menteeService;

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewMenteeAdminSuccess() {
        Mentee mentee = createFirstMentee();
        projectService.save(mentee.getProject());

        MenteeDto menteeDto = new MenteeDto(mentee.getSurname(), mentee.getName(), mentee.getPatronymic(), mentee.getProject().getId());

        mockMvc.perform(post("/mentee/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(menteeDto)))
                .andExpect(status().isOk());


        List<Mentee> dbEntity = menteeService.findAll();
        assertEquals(dbEntity.size(), 1);
        assertEquals(dbEntity.get(0).getSurname(), mentee.getSurname());
        assertEquals(dbEntity.get(0).getName(), mentee.getName());
        assertEquals(dbEntity.get(0).getPatronymic(), mentee.getPatronymic());
        assertEquals(dbEntity.get(0).getProject(), mentee.getProject());
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testAddNewMenteeUserFailure() {
        Mentee mentee = createFirstMentee();

        projectService.save(mentee.getProject());

        menteeService.save(mentee);

        mockMvc.perform(post("/mentee/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentee)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateMenteeAdminSuccess() {
        Mentee mentee = createFirstMentee();

        projectService.save(mentee.getProject());
        menteeService.save(mentee);

        mentee.setName("New name");

        MenteeDto menteeDto = new MenteeDto(mentee.getSurname(), mentee.getName(), mentee.getPatronymic(), mentee.getProject().getId());
        mockMvc.perform(put("/mentee/update")
                        .param("id", mentee.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(menteeDto)))
                .andExpect(status().isOk());

        Mentee dbEntity = menteeService.findById(mentee.getId());
        assertEquals(mentee, dbEntity);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testUpdateMenteeAdminFailure() {
        Mentee mentee = createFirstMentee();
        MenteeDto menteeDto = new MenteeDto(mentee.getSurname(), mentee.getName(), mentee.getPatronymic(), mentee.getProject().getId());

        mockMvc.perform(put("/mentee/update")
                        .param("id", mentee.getId().toString())
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(menteeDto)))
                .andExpect(status().is(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTEE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testUpdateMenteeUserFailure() {
        Mentee mentee = createFirstMentee();

        mockMvc.perform(put("/mentee/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(mentee)))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteMenteeAdminSuccess() {
        Mentee mentee = createFirstMentee();
        projectService.save(mentee.getProject());
        menteeService.save(mentee);

        mockMvc.perform(delete("/mentee/delete")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(menteeService.contains(mentee.getId()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteMenteeAdminFailure() {
        Mentee mentee = createFirstMentee();

        projectService.save(mentee.getProject());

        mockMvc.perform(delete("/mentee/delete")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().is(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTEE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteMenteeUserFailure() {
        Mentee mentee = createFirstMentee();

        projectService.save(mentee.getProject());

        mockMvc.perform(delete("/mentee/delete")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testDeleteAllMenteesAdminSuccess() {
        Mentee mentee1 = createFirstMentee();
        Mentee mentee2 = createSecondMentee();

        projectService.save(mentee1.getProject());
        projectService.save(mentee2.getProject());

        menteeService.save(mentee1);
        menteeService.save(mentee2);

        mockMvc.perform(delete("/mentee/delete-all"))
                .andExpect(status().isOk());

        assertEquals(menteeService.findAll().size(), 0);
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testDeleteAllMenteesUserFailure() {
        Mentee mentee1 = createFirstMentee();
        Mentee mentee2 = createSecondMentee();

        projectService.save(mentee1.getProject());
        projectService.save(mentee2.getProject());

        menteeService.save(mentee1);
        menteeService.save(mentee2);

        mockMvc.perform(delete("/mentee/delete-all"))
                .andExpect(status().is(403));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetMenteeAdminSuccess() {
        Mentee mentee = createFirstMentee();
        projectService.save(mentee.getProject());
        menteeService.save(mentee);
        mockMvc.perform(get("/mentee/get")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(mentee)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetMenteeUserSuccess() {
        Mentee mentee = createFirstMentee();
        projectService.save(mentee.getProject());
        menteeService.save(mentee);
        mockMvc.perform(get("/mentee/get")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(mentee)));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetMenteeAdminFailure() {
        Mentee mentee = createFirstMentee();

        mockMvc.perform(get("/mentee/get")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().is(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTEE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetMenteeUserFailure() {
        Mentee mentee = createFirstMentee();

        mockMvc.perform(get("/mentee/get")
                        .param("id", mentee.getId().toString()))
                .andExpect(status().is(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(MENTEE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(MENTEE_NOT_FOUND.getMessage()));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllMenteesAdminSuccess() {
        Mentee mentee1 = createFirstMentee();

        projectService.save(mentee1.getProject());

        menteeService.save(mentee1);

        mockMvc.perform(get("/mentee/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentee1))));
    }

    @SneakyThrows
    @Test
    @WithMockUser(roles = "USER")
    void testGetAllMenteesUserSuccess() {
        Mentee mentee1 = createFirstMentee();

        projectService.save(mentee1.getProject());

        menteeService.save(mentee1);

        mockMvc.perform(get("/mentee/get-all"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(List.of(mentee1))));
    }

    private Mentee createFirstMentee() {
        Project project = new Project(UUID.randomUUID(), "Project name 1", "Project description 1");
        return new Mentee(UUID.randomUUID(), "Mentee surname 1", "Mentee name 1", "Mentee patronymic 1", project);
    }

    private Mentee createSecondMentee() {
        Project project = new Project(UUID.randomUUID(), "Project name 2", "Project description 2");
        return new Mentee(UUID.randomUUID(), "Mentee surname 2", "Mentee name 2", "Mentee patronymic 2", project);
    }
}
