package ru.mrchuvyzgalov.mentoring;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = AbstractTest.class)
@Testcontainers
public class AbstractTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13");
    @Autowired
    private JdbcTemplate jdbcTemplate;

    static {
        postgres.start();
    }

    @AfterEach
    void resetPostgres() {
        jdbcTemplate.execute("delete from application");
        jdbcTemplate.execute("delete from mentor_time_range");
        jdbcTemplate.execute("delete from mentor_skill");
        jdbcTemplate.execute("delete from mentor");
        jdbcTemplate.execute("delete from time_range");
        jdbcTemplate.execute("delete from mentee");
        jdbcTemplate.execute("delete from project");
        jdbcTemplate.execute("delete from skill");
        jdbcTemplate.execute("delete from category");
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    public static byte[] convertObjectToJsonBytes(Object object) throws RuntimeException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsBytes(object);
        } catch (IOException exc) {
            throw new RuntimeException("Failed to build json: " + exc);
        }
    }
}
